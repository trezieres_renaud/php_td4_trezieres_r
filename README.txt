/!\ Pour la table `critiques`, un script est disponible dans 'database.sql' pour visualiser le résultat. 


1) - autoloader : la ligne $loader->add('', 'src') sert pour l'import des classes contenues dans le dossier 'src'

2) - match() : permet d'appeler une méthode si elle existe

3) - La classe Model sert à modéliser la base de données. Elle permet d'effectuer la préparation de la connexion, de l'éxécuter, de récupérer les éléments si trouvés.

4) - Les variables passées en paramètre à la méthode render() sert à faire le lien avec le controleur (fichier twig) pour l'affichage de la page. 